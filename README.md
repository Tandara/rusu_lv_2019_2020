# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Marko Tandara

LV1
Ispravak counting_wards.py - Dodavanje zagrada kod print funkcija, ispravak naziva fnamex varijable u fname, ispravak if word petlje.
