import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
from sklearn import preprocessing
from sklearn.neural_network import MLPClassifier

def generate_data(n):
    #prva klasa
    n1 = int(n/2)
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

    #druga klasa
    n2 = int(n - n/2)
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)

    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]

    return data

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

np.random.seed(242)
train_data = generate_data(200)
train_data = preprocessing.scale(train_data)
np.random.seed(242)
test_data = generate_data(100)
test_data = preprocessing.scale(test_data)

#Broj neurona u skrivenom sloju koje cemo koristiti
hidden_layer_neurons = [1, 10, 50, 100]

for i in range(len(hidden_layer_neurons)):
    # Ovdje mjenjamo alpha za usporedbu regulacijskih parametara (0.001, 0.01, 0.1, 1, 10)
    clf = MLPClassifier(solver='lbfgs', alpha=10, hidden_layer_sizes=(hidden_layer_neurons[i]))
    clf.fit(train_data[:,0:2],train_data[:,2])

    predicted = clf.predict(test_data[:,0:2])
  
    x_grid, y_grid = np.mgrid[min(train_data[:,0])-0.5:max(train_data[:,0])+0.5:.05,
                              min(train_data[:,1])-0.5:max(train_data[:,1])+0.5:.05]
    grid = np.c_[x_grid.ravel(), y_grid.ravel()]
    probs = clf.predict_proba(grid)[:, 1].reshape(x_grid.shape)
    
    cont = plt.contour(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1, levels=[.5])
    plt.scatter(test_data[:,0],test_data[:,1],c=predicted)
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.title('U skrivenom sloju %d neurona' %hidden_layer_neurons[i])

    plot_confusion_matrix(metrics.confusion_matrix(test_data[:,2], predicted))

    tn, fp, fn, tp = metrics.confusion_matrix(test_data[:, 2], predicted).ravel()

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    missclassification_rate = 1 - accuracy
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    specificity = tn / (tn+fp)

    print('U skrivenom sloju %d neurona:' %hidden_layer_neurons[i])
    print('accuracy = ' + str(accuracy))
    print('missclassification rate = ' +  str(missclassification_rate))
    print('precision = ' + str(precision))
    print('recall/sensitivity = ' + str(recall))
    print('specificity = ' + str(specificity))


