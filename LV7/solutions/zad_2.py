import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

# kreiranje podataka
np.random.seed(242)
train_data = non_func(200)
np.random.seed(242)
test_data = non_func(100)

# izgradnja NN (potrebno mijenjati brojeve neurona i regulacijskog koeficijenta)
NN = MLPRegressor(solver='adam', alpha=1e-4, hidden_layer_sizes=(1000,100), max_iter=1000)
NN.fit(train_data[:,0].reshape(-1,1), train_data[:,2])

#generiranje linearnog regresijskog modela s pol. članovima
poly = PolynomialFeatures(1)
data_train_new = poly.fit_transform(train_data)
data_test_new = poly.fit_transform(test_data)
linReg  = lm.LinearRegression().fit(data_train_new[:,:-1],data_train_new[:,-1])

predicted = NN.predict(test_data[:,0].reshape(-1,1))
predicted_lin = linReg.predict(data_test_new[:,:-1])

#prikaz grafa
plt.scatter(test_data[:,0], test_data[:,2], c='grey')
plt.plot(test_data[:,0], test_data[:,1], c='red', label = 'Real')
plt.plot(test_data[:,0], predicted, c='green', label = 'Neural network')
plt.plot(test_data[:,0], predicted_lin, c='blue', label = 'Linear regresion')
plt.legend()
plt.show()
