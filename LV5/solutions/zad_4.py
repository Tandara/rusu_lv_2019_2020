import matplotlib.image as mpimg
from sklearn import datasets, cluster
import matplotlib.pyplot as plt
import numpy as np

imageNew = mpimg.imread('resources/example_grayscale.png')
    
X = imageNew.reshape((-1, 1))

plt.figure()
plt.imshow(imageNew,  cmap='gray')
plt.title('Originalna slika')

for clusterCount in range(2, 11):
    kmeans = cluster.KMeans(n_clusters=clusterCount, n_init=1)
    kmeans.fit(X) 
    values = kmeans.cluster_centers_.squeeze()
    labels = kmeans.labels_
    imageNew_compressed = np.choose(labels, values)
    imageNew_compressed.shape = imageNew.shape
    plt.figure(clusterCount)
    plt.title('Slika sa %d clustera' %clusterCount)
    plt.imshow(imageNew_compressed,  cmap='gray')
plt.show()