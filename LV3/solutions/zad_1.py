import pandas as pd

mtcars = pd.read_csv('resources/mtcars.csv')
#1. Zadatak
print(mtcars.sort_values(['mpg'], ascending = False).head(5)[['car', 'mpg']])
#2. Zadatak
print(mtcars[mtcars.cyl == 8].sort_values(['mpg']).head(3)[['car', 'mpg', 'cyl']])
#3. Zadatak
print('Srednja potrošnja automobila sa 6 cilindara je %f mpg.' %mtcars[mtcars.cyl == 6].mpg.mean())
#4. Zadatak
print('Srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs je %f mpg' %mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2) & (mtcars.wt <= 2.2)].mpg.mean())
#5. Zadatak
print('Postoji %d automobila s ručnim i %d auta s automatskim mjenjačem.' %(len(mtcars[mtcars.am == 1]), len(mtcars[mtcars.am == 0])))
#6. Zadatak
print('Postoji %d automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga.' %len(mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]))
#7. Zadatak
mtcars['kg'] = mtcars.wt * 453.592
print(mtcars[['car', 'kg']])
