import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv('resources/mtcars.csv')
#1. Zadatak
mtcars.groupby(mtcars.cyl).mpg.mean().plot.bar()
plt.xlabel('Broj cilindara')
plt.ylabel('Prosječna potrošnja')
#2. Zadatak
mtcars.boxplot(column = 'wt', by = ['cyl'])
plt.xlabel('Broj cilindara')
plt.ylabel('Masa')
#3. Zadatak
mtcars.boxplot(column = 'mpg', by = ['am'])
plt.xticks([1, 2], ['Automatski', 'Ručni'])
plt.xlabel('Vrsta mjenjača')
plt.ylabel('Potrošnja')
#4. Zadatak
plt.figure(4)
plt.scatter(mtcars[mtcars.am == 0].hp, mtcars[mtcars.am == 0].qsec, label = 'Automatski')
plt.scatter(mtcars[mtcars.am == 1].hp, mtcars[mtcars.am == 1].qsec, label = 'Ručni')
plt.xlabel('Konjske snage')
plt.ylabel('Ubrzanje')
plt.legend()
# Prikaz svih grafova
plt.show()