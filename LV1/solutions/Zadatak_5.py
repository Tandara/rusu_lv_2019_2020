file_name = input('Unesite ime datoteke: ')  
file = open(file_name)

spam = []
for line in file:
    if line.startswith("X-DSPAM-Confidence: "):
        lineInParts = line.split('X-DSPAM-Confidence: ')
        spam.append(float(lineInParts[1]))

print('Average X-DSPAM-Confidence: ', sum(spam) / len(spam))
