brojevi = []

while(True):
    broj = input("Unesite broj: ")
    if broj == "Done":
        break
    try:
        brojevi.append(int(broj))
    except:
        print("Pogresan unos!")

print("\nUneseno je", len(brojevi) ,"broja.")
print("\nMinimalan broj u nizu je", min(brojevi))
print("\nMaksimalan broj u nizu je", max(brojevi))
print("\nSrednja vrijednost unesenog niza je", float(sum(brojevi)/len(brojevi)))
