# -*- coding: utf-8 -*-

broj = float(input("Enter number between 0.0 - 1.0: "))
try:
    broj > 0.0 and broj < 1.0 
except:
    print ("Number out of bonds.")
    exit()

if broj >= 0.9:
    print('You got A.')
elif broj >= 0.8:
    print('You got B.')
elif broj >= 0.7:
    print('You got C.')
elif broj >= 0.6:
    print('You got D.')
elif broj < 0.6:
    print('You got F.')