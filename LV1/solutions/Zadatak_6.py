file_name = input("Unesite ime datoteke:")  
file = open(file_name)

Emails = []
for line in file:
    if line.startswith("From:") and "@" in line:
        partsOfLine = line.strip()
        partsOfLine = partsOfLine.split('From: ')
        Emails.append(partsOfLine[1])

Hostnames = {}
for email in Emails:   
    name = email.split("@")
    host = name[1]
    if host not in Hostnames:
        Hostnames[host] = 1
    Hostnames[host] += 1

print(Emails[:10])
print(Hostnames)
