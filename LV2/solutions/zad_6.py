import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

img = mpimg.imread('../resources/tiger.png')
print(img)
print(img.shape)
gray = np.dot(img[..., :3], [0.299, 0.587, 0.144])
gray_enhanced = np.zeros((640, 960))
print(gray)

for i in range(img.shape(0)):
    for j in range(img.shape(1)):
        gray_enhanced[i, j] = gray[i, j] + 110.5

plt.figure(1)
plt.imshow(gray)

plt.figure(2)
plt.imshow(gray_enhanced)

plt.show()
