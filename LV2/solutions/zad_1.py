import re

file = open('../resources/mbox-short.txt')

email_lst = []

for line in file:
    emails = re.findall('[a-zA-Z0-9]+[\/._-]*[a-zA-Z0-9]+@', line)
    for email in emails:
        email_lst.append(email.split('@')[0])
    
print(email_lst)
