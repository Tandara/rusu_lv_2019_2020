import re

file = open('../resources/mbox-short.txt')

email_lst = []

for line in file:
    emails = re.findall('[a-zA-Z0-9]+[\/._-]*[a-zA-Z0-9]+@', line)
    for email in emails:
        email_lst.append(email.split('@')[0])

user=' '.join(email_lst)

zad_2_1 = re.findall('\S*a\S*', user)
print(zad_2_1)
print(len(zad_2_1))

just_one_a = re.compile('^[^a]*a[^a]*$')
zad_2_2 = list(filter(just_one_a.match, email_lst))
print(zad_2_2)
print(len(zad_2_2))

without_a = re.compile('^[^a]+$')
zad_2_3 = list(filter(without_a.match, email_lst))
print(zad_2_3)
print(len(zad_2_3))

numbers = re.compile('\S*[0-9]\S*')
zad_2_4 = list(filter(numbers.match, email_lst))
print(zad_2_4)
print(len(zad_2_4))

low_letters = re.compile('^[a-z]+$')
zad_2_5 = list(filter(low_letters.match, email_lst))
print(zad_2_5)
print(len(zad_2_5))
