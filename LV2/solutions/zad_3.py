import numpy as np
import matplotlib.pyplot as plt

np.random.seed(56) 
rNumbers = np.random.randint(2, size=100)

mu = 187
sigma = 2
v = np.random.normal(mu,sigma,10000)

visine_m = []
visine_z = []

for i in rNumbers:
    if i == 0:
        visine_z.append(np.random.normal(167, 7))
    else:
        visine_m.append(np.random.normal(180, 7))

plt.hist([visine_m, visine_z], color = ['blue', 'red'])

avg_m = np.average(visine_m)
avg_z = np.average(visine_z)

plt.axvline(avg_m, color='purple')
plt.axvline(avg_z, color='black')
plt.legend(["m", "z", "avg_m", "avg_z"])
