import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y
 
def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis] 

x_ones = np.ones((len(x), 1))
x = np.append(x_ones, x, axis = 1)

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

transp = np.transpose(xtrain)
theta = np.linalg.inv(transp @ xtrain) @ transp @ ytrain
print(theta)

